﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace metodosassincronos
{
    class Program
    {
        static readonly HttpClient cliente = new HttpClient();
        static async Task Main(string[] args)
        {
            try
            {
                var resposta = await cliente.GetAsync("https://reqres.in/api/users/2");
                if (resposta.IsSuccessStatusCode)
                {
                    string dadosJson = await resposta.Content.ReadAsStringAsync();
                    Console.WriteLine(dadosJson);
                    var dados = JsonSerializer.Deserialize<DadosDTO>(dadosJson);
                    Console.WriteLine(dados.data.email);
                }
            }
            catch(HttpRequestException e)
            {
                Console.WriteLine("Falha no consumo do web service:");
                Console.WriteLine(e.Message);
            }
        }
    }
}
