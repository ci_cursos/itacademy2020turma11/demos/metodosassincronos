namespace metodosassincronos
{
    public class DadosDTO
    {
        public UsuarioDTO data { get; set; }
        public AdDTO ad {get;set;}
    }

    public class UsuarioDTO
    {
        public int id { get; set; } 
        public string email { get; set; } 
        public string first_name { get; set; } 
        public string last_name { get; set; } 
        public string avatar { get; set; } 
    }

    public class AdDTO
    {
        public string company { get; set; } 
        public string url { get; set; } 
        public string text { get; set; }
    }
}